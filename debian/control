Source: coinor-cgl
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Pierre Gruet <pgt@debian.org>
Section: science
Priority: optional
Build-Depends: coinor-libclp-dev (>= 1.17.9+ds),
               coinor-libcoinutils-dev,
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               libbz2-dev,
               liblapack-dev,
               pkgconf,
               zlib1g-dev,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/coinor-cgl
Vcs-Git: https://salsa.debian.org/science-team/coinor-cgl.git
Homepage: https://projects.coin-or.org/Cgl
Rules-Requires-Root: no

Package: coinor-libcgl1
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: COIN-OR Cut Generation Library
 The Cut Generation Library (Cgl) is an open collection of cutting plane
 implementations ("cut generators") for use in teaching, research, and
 applications.
 .
 Cgl is part of the larger COIN-OR initiative (Computational Infrastructure
 for Operations Research) and can be used with other COIN-OR packages that
 make use of cuts, such as the mixed-integer linear programming solver Cbc.
 .
 This package contains the binaries and libraries.

Package: coinor-libcgl-dev
Architecture: any
Section: libdevel
Depends: coinor-libcgl1 (= ${binary:Version}),
         coinor-libclp-dev (>= 1.17.9+ds),
         coinor-libcoinutils-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: COIN-OR Cut Generation Library (developer files)
 The Cut Generation Library (Cgl) is an open collection of cutting plane
 implementations ("cut generators") for use in teaching, research, and
 applications.
 .
 Cgl is part of the larger COIN-OR initiative (Computational Infrastructure
 for Operations Research) and can be used with other COIN-OR packages that
 make use of cuts, such as the mixed-integer linear programming solver Cbc.
 .
 This package contains the header files for developers.

Package: coinor-libcgl-doc
Architecture: all
Section: doc
Depends: libjs-jquery,
         ${misc:Depends}
Recommends: coinor-libcgl-dev
Multi-Arch: foreign
Description: COIN-OR Cut Generation Library (documentation)
 The Cut Generation Library (Cgl) is an open collection of cutting plane
 implementations ("cut generators") for use in teaching, research, and
 applications.
 .
 Cgl is part of the larger COIN-OR initiative (Computational Infrastructure
 for Operations Research) and can be used with other COIN-OR packages that
 make use of cuts, such as the mixed-integer linear programming solver Cbc.
 .
 This package contains the documentation and examples.
